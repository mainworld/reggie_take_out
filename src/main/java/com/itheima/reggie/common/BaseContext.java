package com.itheima.reggie.common;

/**
 * @program: reggie_take_out
 * @description: 基于ThreadLocal封装工具类，用户保存和获取当前登录用户id
 * @author: zzw
 * @create: 2022-07-23 16:13
 */
public class BaseContext {
    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    /**
     * 获取值
     *
     * @return
     */
    public static Long getCurrentId() {
        return threadLocal.get();
    }

    /**
     * 设置值
     *
     * @param id
     */
    public static void setCurrentId(Long id) {
        threadLocal.set(id);
    }
}

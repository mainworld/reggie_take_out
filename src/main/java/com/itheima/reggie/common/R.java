package com.itheima.reggie.common;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 通用返回结果，服务端响应的数据最终都会封装为此对象
 *
 * @param <T>
 */
@Data
@ApiModel("通用返回结果，服务端响应的数据最终都会封装为此对象")
public class R<T> {

    /**
     * 编码：1成功，0和其它数字为失败
     */
    @ApiModelProperty(value = "编码：1成功，0和其它数字为失败",dataType = "Integer",name = "code")
    private Integer code;

    /**
     * 错误信息
     */
    @ApiModelProperty(value = "错误信息",dataType = "String",name = "msg")
    private String msg;

    /**
     * 数据
     */
    @ApiModelProperty(value = "数据",dataType = "T",name = "data")
    private T data;

    /**
     * 动态数据
     */
    @ApiModelProperty(value = "动态数据",dataType = "HashMap",name = "map")
    private Map map = new HashMap();

    /**
     * 设置后台处理成功时的响应数据
     * @param object
     * @param <T>
     * @return
     */
    @ApiOperation(value = "success",notes = "设置后台处理成功时的响应数据")
    public static <T> R<T> success(T object) {
        R<T> r = new R<T>();
        r.data = object;
        r.code = 1;
        return r;
    }

    /**
     * 设置后台处理失败时的响应数据
     * @param msg
     * @param <T>
     * @return
     */
    @ApiOperation(value = "error",notes = "设置后台处理失败时的响应数据")
    public static <T> R<T> error(String msg) {
        R r = new R();
        r.msg = msg;
        r.code = 0;
        return r;
    }

    /**
     * 添加动态数据
     * @param key
     * @param value
     * @return
     */
    @ApiOperation(value = "add",notes = "添加动态数据")
    public R<T> add(String key, Object value) {
        this.map.put(key, value);
        return this;
    }

}

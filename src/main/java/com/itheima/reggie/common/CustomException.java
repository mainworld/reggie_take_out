package com.itheima.reggie.common;

/**
 * @program: reggie_take_out
 * @description: 自定义业务异常
 * @author: zzw
 * @create: 2022-07-24 00:01
 */
public class CustomException extends RuntimeException {
    public CustomException(String message) {
        super(message);
    }
}

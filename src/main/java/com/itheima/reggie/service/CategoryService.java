package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Category;

import java.util.List;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与分类表的映射
 * @author: zzw
 * @create: 2022-07-23 16:35
 */
public interface CategoryService extends IService<Category> {
    /**
     * 根据id删除分类，删除之前需进行判断
     * @param id
     */
    void remove(Long id);

    /**
     * 分页查询
     * @param page
     * @param pageSize
     * @return
     */
    Page<Category> pageSelect(int page, int pageSize);

    /**
     * 根据条件查询分类数据
     * @param category
     * @return
     */
    List<Category> listSelect(Category category);
}

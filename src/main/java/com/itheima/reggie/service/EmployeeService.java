package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Employee;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与员工表的映射
 * @author: zzw
 * @create: 2022-07-22 16:23
 */
public interface EmployeeService extends IService<Employee> {
    /**
     * 员工信息分页查询
     * @param page
     * @param pageSize
     * @param name
     */
    Page pageSelect(int page, int pageSize, String name);
}

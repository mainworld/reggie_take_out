package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.SetmealDish;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与套餐菜品关系表的映射
 * @author: zzw
 * @create: 2022-07-26 14:57
 */
public interface SetmealDishService extends IService<SetmealDish> {
}

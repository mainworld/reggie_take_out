package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.Orders;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与订单表的映射
 * @author: zzw
 * @create: 2022-07-27 16:25
 */
public interface OrderService extends IService<Orders> {
    /**
     * 用户下单
     * @param orders
     */
    void submit(Orders orders);

    /**
     * 派送订单
     * @param orders
     */
    void delivery(Orders orders);

    /**
     * 订单信息查询
     * @param page
     * @param pageSize
     * @param beginTime
     * @param endTime
     * @return
     */
    Page<Orders> pageSelect(int page, int pageSize, String beginTime, String endTime);

    /**
     * 用户订单分页查询
     * @param page
     * @param pageSize
     * @return
     */
    Page<Orders> userPage(int page, int pageSize);
}

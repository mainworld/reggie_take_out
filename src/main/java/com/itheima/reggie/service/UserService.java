package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.User;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与用户表的映射
 * @author: zzw
 * @create: 2022-07-27 10:20
 */
public interface UserService extends IService<User> {
    /**
     * 移动端用户登录
     * @param user
     * @param request
     * @return
     */
    User login(User user, HttpServletRequest request);
}

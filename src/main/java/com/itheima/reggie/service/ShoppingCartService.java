package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.ShoppingCart;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与购物车表的映射
 * @author: zzw
 * @create: 2022-07-27 11:35
 */
public interface ShoppingCartService extends IService<ShoppingCart> {
    /**
     * 获取购物车列表
     * @param request
     * @return
     */
    List<ShoppingCart> listSelect(HttpServletRequest request);

    /**
     * 购物车添加菜品
     * @param shoppingCart
     * @return
     */
    ShoppingCart addNew(ShoppingCart shoppingCart);

    /**
     * 删除购物车中菜品或者套餐
     * @param shoppingCart
     * @return
     */
    ShoppingCart sub(ShoppingCart shoppingCart);

    /**
     * 清空购物车
     */
    void clean();
}

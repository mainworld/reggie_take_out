package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.OrderDetail;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与订单明细表的映射
 * @author: zzw
 * @create: 2022-07-27 16:27
 */
public interface OrderDetailService extends IService<OrderDetail> {
}

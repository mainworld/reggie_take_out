package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;

import java.util.List;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与菜品表的映射
 * @author: zzw
 * @create: 2022-07-23 23:27
 */
public interface DishService extends IService<Dish> {
    /**
     * 新增菜品，同时插入菜品对应的口味数据，需要操作两张表：dish，dish_flavor
     * @param dishDto
     */
    void saveWithFlavor(DishDto dishDto);

    /**
     * 根据id查询菜品信息和对应的口味信息
     * @param id
     * @return
     */
    DishDto getByIdWithFlavor(Long id);

    /**
     * 更新菜品信息，同时更新对应的口味信息
     * @param dishDto
     */
    void updateWithFlavor(DishDto dishDto);

    /**
     * 删除菜品信息，删除之前先进行判断
     * @param ids
     */
    void remove(List<Long> ids);

    /**
     * 根据条件查询对应的菜品数据
     * @param dish
     * @return
     */
    List<DishDto> listSelect(Dish dish);

    /**
     * 菜品信息分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    Page<DishDto> pageSelect(int page,int pageSize,String name);

    /**
     * 根据id停售或启售菜品
     * @param status
     * @param ids
     */
    void updateStatusByIds(Integer status, List<Long> ids);
}

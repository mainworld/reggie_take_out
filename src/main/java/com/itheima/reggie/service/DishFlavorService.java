package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.DishFlavor;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与菜品口味表的映射
 * @author: zzw
 * @create: 2022-07-25 14:41
 */
public interface DishFlavorService extends IService<DishFlavor> {
}

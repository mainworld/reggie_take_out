package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.entity.AddressBook;

import java.util.List;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与地址簿表的映射
 * @author: zzw
 * @create: 2022-07-27 15:06
 */
public interface AddressBookService extends IService<AddressBook> {
    /**
     * 查询默认地址
     * @return
     */
    AddressBook getDefault();

    /**
     * 设置默认地址
     * @return
     */
    void setDefault(AddressBook addressBook);

    /**
     * 查询指定用户的全部地址
     * @param addressBook
     * @return
     */
    List<AddressBook> listSelect(AddressBook addressBook);
}

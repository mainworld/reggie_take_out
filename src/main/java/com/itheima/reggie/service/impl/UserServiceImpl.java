package com.itheima.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.mapper.UserMapper;
import com.itheima.reggie.service.UserService;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: reggie_take_out
 * @description:
 * @author: zzw
 * @create: 2022-07-27 10:20
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    /**
     * 移动端用户登录
     * @param user
     * @param request
     * @return
     */
    @Override
    public User login(User user, HttpServletRequest request) {
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getPhone, user.getPhone());
        User userServiceOne = this.getOne(queryWrapper);
        if (userServiceOne == null) {
            userServiceOne = new User();
            userServiceOne.setPhone(user.getPhone());
            userServiceOne.setStatus(1);
            this.save(userServiceOne);
        }
        return userServiceOne;
    }
}

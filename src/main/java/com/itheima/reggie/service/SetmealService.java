package com.itheima.reggie.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;

import java.util.List;

/**
 * @program: reggie_take_out
 * @description:  处理实体类与套餐表的映射
 * @author: zzw
 * @create: 2022-07-23 23:23
 */
public interface SetmealService extends IService<Setmeal> {
    /**
     * 新增套餐，同时需要保存套餐和菜品的管理关系
     * @param setmealDto
     */
    void saveWithDish(SetmealDto setmealDto);

    /**
     * 删除套餐，同时需要删除套餐和菜品的关联数据
     * @param ids
     */
    void removeWithDish(List<Long> ids);

    /**
     * 套餐分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    Page<SetmealDto> pageSelect(int page, int pageSize, String name);

    /**
     * 根据添加查询套餐数据
     * @param setmeal
     * @return
     */
    List<Setmeal> listSelect(Setmeal setmeal);

    /**
     * 根据id查询套餐信息
     * @param id
     * @return
     */
    SetmealDto getByIdWithDish(Long id);

    /**
     * 修改套餐信息
     * @param setmealDto
     */
    void updateWithDish(SetmealDto setmealDto);

    /**
     * 根据id停售或启售套餐
     * @param ids
     */
    void updateStatusByIds(Integer status,List<Long> ids);
}

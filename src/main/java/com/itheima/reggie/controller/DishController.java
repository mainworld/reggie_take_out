package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.DishDto;
import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.service.DishService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: reggie_take_out
 * @description: 菜品管理
 * @author: zzw
 * @create: 2022-07-25 14:43
 */
@RestController
@RequestMapping("/dish")
@Slf4j
@Api("菜品管理API接口")
public class DishController {
    @Autowired
    private DishService dishService;

    /**
     * 新增菜品
     * @param dishDto
     * @return
     */
    @PostMapping
    @ApiOperation(value = "新增菜品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dishDto",value = "dishDto",required = true,dataType = "DishDto")
    })
    public R<String> save(@RequestBody DishDto dishDto){
        log.info(dishDto.toString());
        dishService.saveWithFlavor(dishDto);
        return R.success("新增菜品成功");
    }

    /**
     *  菜品信息分页查询
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "菜品信息分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "page",required = true,dataType = "Integer",example = "1"),
            @ApiImplicitParam(name = "pageSize",value = "pageSize",required = true,dataType = "Integer",example = "10"),
            @ApiImplicitParam(name = "name",value = "name",required = true,dataType = "String",example = "")
    })
    public R<Page> page(int page,int pageSize,String name){
        Page<DishDto> dishDtoPage=dishService.pageSelect(page, pageSize, name);
        return R.success(dishDtoPage);
    }

    /**
     * 根据id查询菜品信息和对应的口味数据
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "根据id查询菜品信息和对应的口味数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "id",required = true,dataType = "Long",paramType = "path")
    })
    public R<DishDto> get(@PathVariable Long id){
        DishDto dishDto=dishService.getByIdWithFlavor(id);
        return R.success(dishDto);
    }

    /**
     * 修改菜品
     * @param dishDto
     * @return
     */
    @PutMapping
    @ApiOperation(value = "修改菜品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dishDto",value = "dishDto",required = true,dataType = "DishDto")
    })
    public R<String> update(@RequestBody DishDto dishDto){
        log.info(dishDto.toString());
        dishService.updateWithFlavor(dishDto);
        return R.success("修改菜品成功");
    }

    /**
     * 根据条件查询对应的菜品数据
     * @param dish
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "根据条件查询对应的菜品数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "dish",value = "dish",required = true,dataType = "Dish")
    })
    public R<List<DishDto>> list(Dish dish){
        List<DishDto> dishDtoList=dishService.listSelect(dish);
        return R.success(dishDtoList);
    }

    /**
     * 删除菜品
     * @param ids
     * @return
     */
    @DeleteMapping
    @GetMapping("/list")
    @ApiOperation(value = "删除菜品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids",value = "ids",required = true,dataType = "List<Long>")
    })
    public R<String> del(@RequestParam List<Long> ids){
        log.info("删除菜品,id为:{}",ids);
        dishService.remove(ids);
        return R.success("菜品信息删除成功");
    }

    /**
     * 根据id停售或启售菜品
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    @ApiOperation(value = "根据id停售或启售菜品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status",value = "status",required = true,dataType = "Integer",paramType = "path"),
            @ApiImplicitParam(name = "ids",value = "ids",required = true,dataType = "List<Long>")
    })
    public R<String> status(@PathVariable Integer status, @RequestParam List<Long> ids){
        log.info("status:{}  ids:{}",status,ids);
        dishService.updateStatusByIds(status,ids);
        return R.success("菜品状态更新成功");
    }
}

package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.dto.SetmealDto;
import com.itheima.reggie.entity.Setmeal;
import com.itheima.reggie.service.CategoryService;
import com.itheima.reggie.service.SetmealDishService;
import com.itheima.reggie.service.SetmealService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: reggie_take_out
 * @description: 套餐管理
 * @author: zzw
 * @create: 2022-07-26 15:00
 */
@RestController
@RequestMapping("/setmeal")
@Slf4j
@Api("套餐管理API接口")
public class SetmealController {
    @Autowired
    private SetmealDishService setmealDishService;
    @Autowired
    private SetmealService setmealService;
    @Autowired
    private CategoryService categoryService;

    /**
     * 新增套餐
     *
     * @param setmealDto
     * @return
     */
    @PostMapping
    @ApiOperation(value = "新增套餐")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "setmealDto",value = "setmealDto",required = true,dataType = "SetmealDto")
    })
    public R<String> save(@RequestBody SetmealDto setmealDto) {
        log.info("套餐信息：{}", setmealDto);
        setmealService.saveWithDish(setmealDto);
        return R.success("新增套餐成功");
    }

    /**
     * 套餐分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "套餐分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "page",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize",value = "pageSize",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "name",value = "name",required = true,dataType = "String")
    })
    public R<Page> page(int page, int pageSize, String name) {
        Page<SetmealDto> dtoPage = setmealService.pageSelect(page, pageSize, name);
        return R.success(dtoPage);
    }

    /**
     * 删除套餐
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation(value = "删除套餐")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids",value = "ids",required = true,dataType = "List<Long>")
    })
    public R<String> delete(@RequestParam List<Long> ids) {
        log.info("ids:{}", ids);
        setmealService.removeWithDish(ids);
        return R.success("套餐数据删除成功");
    }

    /**
     * 根据添加查询套餐数据
     *
     * @param setmeal
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "根据添加查询套餐数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "setmeal",value = "setmeal",required = true,dataType = "Setmeal")
    })
    public R<List<Setmeal>> list(Setmeal setmeal) {
        List<Setmeal> list = setmealService.listSelect(setmeal);
        return R.success(list);
    }

    /**
     * 根据id查询套餐信息
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "根据id查询套餐信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "id",required = true,dataType = "Long",paramType = "path")
    })
    public R<SetmealDto> getById(@PathVariable Long id){
        SetmealDto setmealDto=setmealService.getByIdWithDish(id);
        return R.success(setmealDto);
    }

    /**
     * 修改套餐信息。
     *
     * @param setmealDto
     * @return
     */
    @PutMapping
    @ApiOperation(value = "修改套餐信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "setmealDto",value = "setmealDto",required = true,dataType = "SetmealDto")
    })
    public R<String> update(@RequestBody SetmealDto setmealDto) {
        log.info("修改套餐信息{}", setmealDto);
        // 执行更新。
        setmealService.updateWithDish(setmealDto);
        return R.success("修改套餐信息成功");
    }



    /**
     * 根据id停售或启售套餐
     * @param status
     * @param ids
     * @return
     */
    @PostMapping("/status/{status}")
    @ApiOperation(value = "根据id停售或启售套餐")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "status",value = "status",required = true,dataType = "Integer",paramType = "path"),
            @ApiImplicitParam(name = "ids",value = "ids",required = true,dataType = "List<Long>")
    })
    public R<String> status(@PathVariable Integer status, @RequestParam List<Long> ids){
        log.info("status:{}  ids:{}",status,ids);
//        Setmeal setmeal=setmealService.getById(ids);
//        setmeal.setStatus(status);
//        setmealService.saveOrUpdate(setmeal);
        setmealService.updateStatusByIds(status,ids);
        return R.success("套餐状态更新成功");
    }
}

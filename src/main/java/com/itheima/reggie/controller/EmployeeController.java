package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Employee;
import com.itheima.reggie.service.EmployeeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * @program: reggie_take_out
 * @description: 员工管理
 * @author: zzw
 * @create: 2022-07-22 16:26
 */
@Slf4j
@RestController
@RequestMapping("/employee")
@Api("员工管理API接口")
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;

    /**
     * 员工登录
     *
     * @param employee
     * @param request
     * @return
     */
    @PostMapping("/login")
    @ApiOperation(value = "员工登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "employee",value = "Employee",required = true,dataType = "Employee"),
            @ApiImplicitParam(name = "request",value = "request",required = true,dataType = "HttpServletRequest")
    })
    public R<Employee> login(@RequestBody Employee employee, HttpServletRequest request) {
        //1.将页面提交的密码password进行MD5加密处理
        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        //2.根据页面提交的用户名username查询数据库
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Employee::getUsername, employee.getUsername());
        Employee emp = employeeService.getOne(queryWrapper);
        //3.如果没有查询到则返回登录失败结果
        if (emp == null) {
            return R.error("登录失败");
        }
        //4.密码比对，如果不一致则返回登录失败结果
        if (!emp.getPassword().equals(password)) {
            return R.error("登录失败");
        }
        //5.查看员工状态，如果为已禁用状态，则返回员工已禁用结果
        if (emp.getStatus() == 0) {
            return R.error("账号已禁用");
        }
        //6.登陆成功，将员工id存入Session并返回登录成功结果
        request.getSession().setAttribute("employee", emp.getId());
        return R.success(emp);
    }

    /**
     * 员工退出
     *
     * @param request
     * @return
     */
    @RequestMapping("/logout")
    @ApiOperation(value = "员工退出")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "request",value = "request",required = true,dataType = "HttpServletRequest")
    })
    public R<String> logout(HttpServletRequest request) {
        //清除Session中保存的当前登录员工的id
        request.getSession().removeAttribute("employee");
        return R.success("退出成功");
    }

    /**
     * 新增员工
     *
     * @param employee
     * @return
     */
    @PostMapping
    @ApiOperation(value = "新增员工")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "request",value = "request",required = true,dataType = "HttpServletRequest"),
            @ApiImplicitParam(name = "employee",value = "employee",required = true,dataType = "Employee")
    })
    public R<String> save(HttpServletRequest request,@RequestBody Employee employee) {
        log.info("新增员工，员工信息：{}", employee.toString());
        //设置初始密码123456，需要进行MD5加密
        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());

        //获得当前登录用户的id
//        Long empid=(Long)request.getSession().getAttribute("employee");

//        employee.setCreateUser(empid);
//        employee.setUpdateUser(empid);
        employeeService.save(employee);
        return R.success("新增员工成功");
    }

    /**
     * 员工信息分页查询
     *
     * @param page
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "员工信息分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "page",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize",value = "pageSize",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "name",value = "name",required = true,dataType = "String")
    })
    public R<Page> page(int page, int pageSize, String name) {
        log.info("page={},pageSize={},name={}", page, pageSize, name);
        Page pageInfo = employeeService.pageSelect(page, pageSize, name);
        return R.success(pageInfo);
    }

    /**
     * 根据id修改员工信息
     *
     * @param request
     * @param employee
     * @return
     */
    @PutMapping
    @ApiOperation(value = "根据id修改员工信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "request",value = "request",required = true,dataType = "HttpServletRequest"),
            @ApiImplicitParam(name = "employee",value = "employee",required = true,dataType = "Employee")
    })
    public R<String> update(HttpServletRequest request, Employee employee) {
        log.info(employee.toString());

        Long id = Thread.currentThread().getId();
        log.info("线程id为：{}", id);
//        Long empId=(Long)request.getSession().getAttribute("employee");
//        employee.setUpdateTime(LocalDateTime.now());
//        employee.setUpdateUser(empId);
        employeeService.updateById(employee);
        return R.success("员工信息修改成功");
    }

    /**
     * 根据id查询员工信息
     *
     * @param id
     * @return
     */
    @GetMapping("/{id}")
    @ApiOperation(value = "根据id查询员工信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "id",value = "id",required = true,dataType = "Long",paramType = "path")
    })
    public R<Employee> getById(@PathVariable Long id) {
        log.info("根据id查询员工信息...");
        Employee employee = employeeService.getById(id);
        if (employee != null) {
            return R.success(employee);
        }
        return R.error("没有查询到对于员工信息");
    }
}

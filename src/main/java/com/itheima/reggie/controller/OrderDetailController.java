package com.itheima.reggie.controller;

import com.itheima.reggie.service.OrderDetailService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @program: reggie_take_out
 * @description:  订单明细管理
 * @author: zzw
 * @create: 2022-07-27 16:32
 */
@RestController
@RequestMapping("/orderDetail")
@Slf4j
@Api("订单明细管理API接口")
public class OrderDetailController {
    @Autowired
    private OrderDetailService orderDetailService;
}

package com.itheima.reggie.controller;

import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.User;
import com.itheima.reggie.service.UserService;
import com.itheima.reggie.utils.SMSUtils;
import com.itheima.reggie.utils.ValidateCodeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * @program: reggie_take_out
 * @description:  用户管理
 * @author: zzw
 * @create: 2022-07-27 10:21
 */
@RestController
@RequestMapping("/user")
@Slf4j
@Api("用户管理API接口")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * 发送手机短信验证码
     *
     * @param user
     * @return
     */
    @PostMapping("/sendMsg")
    @ApiOperation(value = "发送手机短信验证码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user",value = "user",required = true,dataType = "User"),
            @ApiImplicitParam(name = "session",value = "session",required = true,dataType = "HttpSession")
    })
    public R<String> sendMsg(@RequestBody User user, HttpSession session) {
        //获取手机号
        String phone = user.getPhone();
        if (StringUtils.isNotEmpty(phone)) {
            //生成随机的4位验证码
            String code = ValidateCodeUtils.generateValidateCode(4).toString();
            log.info("code：{}", code);
            //调用阿里云提供的短信服务API完成发送短信
            SMSUtils.sendMessage("瑞吉外卖", "", phone, code);
            //需要将生成的验证码保存到Session
            session.setAttribute(phone, code);
            return R.success("手机验证码短信发送成功");
        }
        return R.error("手机验证码短信发送失败");
    }

    /**
     * 移动端用户登录
     *
     * @param user
     * @param request
     * @return
     */
    @PostMapping("/login")
    @ApiOperation(value = "移动端用户登录")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "user",value = "User",required = true,dataType = "User"),
            @ApiImplicitParam(name = "request",value = "request",required = true,dataType = "HttpServletRequest")
    })
    public R<User> login(@RequestBody User user, HttpServletRequest request) {
        User userServiceOne = userService.login(user, request);
        if (userServiceOne.getStatus() == 0) {
            return R.error("该账号已禁用");
        }
        request.getSession().setAttribute("user", userServiceOne.getId());
        return R.success(userServiceOne);
    }

    /**
     * 用户退出
     *
     * @param request
     * @return
     */
    @PostMapping("/loginout")
    @ApiOperation(value = "用户退出")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "request",value = "request",required = true,dataType = "HttpServletRequest")
    })
    public R<String> loginout(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        return R.success("退出账号成功");
    }
}

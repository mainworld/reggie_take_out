package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Category;
import com.itheima.reggie.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @program: reggie_take_out
 * @description: 分类管理
 * @author: zzw
 * @create: 2022-07-23 16:33
 */
@RestController
@RequestMapping("/category")
@Slf4j
@Api("分类管理API接口")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    /**
     * 新增分类
     *
     * @param category
     * @return
     */
    @PostMapping
    @ApiOperation(value = "新增分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category",value = "category",required = true,dataType = "Category")
    })
    public R<String> save(@RequestBody Category category) {
        log.info("category:{}", category);
        categoryService.save(category);
        return R.success("新增分类成功");
    }

    /**
     * 分页查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "page",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize",value = "pageSize",required = true,dataType = "Integer")
    })
    public R<Page> page(int page, int pageSize) {
        Page<Category> pageInfo = categoryService.pageSelect(page, pageSize);
        return R.success(pageInfo);
    }

    /**
     * 根据id删除分类
     *
     * @param ids
     * @return
     */
    @DeleteMapping
    @ApiOperation(value = "根据id删除分类")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "ids",value = "ids",required = true,dataType = "Long")
    })
    public R<String> delete(Long ids) {
        log.info("删除分类,id为:{}", ids);

//        categoryService.removeById(ids);
        categoryService.remove(ids);
        return R.success("分类信息删除成功");
    }

    /**
     * 根据id修改分类信息
     *
     * @param category
     * @return
     */
    @PutMapping
    @ApiOperation(value = "根据id修改分类信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category",value = "category",required = true,dataType = "Category")
    })
    public R<String> update(@RequestBody Category category) {
        log.info("修改分类信息：{}", category);
        categoryService.updateById(category);
        return R.success("修改分类信息成功");
    }

    /**
     * 根据条件查询分类数据
     *
     * @param category
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "根据条件查询分类数据")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "category",value = "category",required = true,dataType = "Category")
    })
    public R<List<Category>> list(Category category) {
        List<Category> list = categoryService.listSelect(category);
        return R.success(list);
    }
}

package com.itheima.reggie.controller;

import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.ShoppingCart;
import com.itheima.reggie.service.ShoppingCartService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @program: reggie_take_out
 * @description: 购物车
 * @author: zzw
 * @create: 2022-07-27 11:37
 */
@RestController
@RequestMapping("/shoppingCart")
@Slf4j
@Api("购物车API接口")
public class ShoppingCartController {
    @Autowired
    private ShoppingCartService shoppingCartService;

    /**
     * 获取购物车列表
     *
     * @param request
     * @return
     */
    @GetMapping("/list")
    @ApiOperation(value = "获取购物车列表")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "request",value = "request",required = true,dataType = "HttpServletRequest")
    )
    public R<List<ShoppingCart>> list(HttpServletRequest request) {
        List<ShoppingCart> list = shoppingCartService.listSelect(request);
        return R.success(list);
    }

    /**
     * 购物车添加菜品
     *
     * @return
     */
    @PostMapping("/add")
    @ApiOperation(value = "购物车添加菜品")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "shoppingCart",value = "shoppingCart",required = true,dataType = "ShoppingCart")
    )
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart) {
        log.info(shoppingCart.toString());
        ShoppingCart cartServiceOne = shoppingCartService.addNew(shoppingCart);
        return R.success(cartServiceOne);
    }

    /**
     * 删除购物车中菜品或者套餐
     *
     * @param shoppingCart
     * @return
     */
    @PostMapping("/sub")
    @ApiOperation(value = "删除购物车中菜品或者套餐")
    @ApiImplicitParams(
            @ApiImplicitParam(name = "shoppingCart",value = "shoppingCart",required = true,dataType = "ShoppingCart")
    )
    public R<ShoppingCart> sub(@RequestBody ShoppingCart shoppingCart) {
        ShoppingCart cartServiceOne = shoppingCartService.sub(shoppingCart);
        return R.success(cartServiceOne);
    }

    /**
     * 清空购物车
     *
     * @return
     */
    @DeleteMapping("/clean")
    @ApiOperation(value = "清空购物车")
    public R<String> clean() {
        shoppingCartService.clean();
        return R.success("购物车清空成功");
    }
}

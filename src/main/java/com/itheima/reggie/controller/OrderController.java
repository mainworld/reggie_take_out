package com.itheima.reggie.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.itheima.reggie.common.R;
import com.itheima.reggie.entity.Orders;
import com.itheima.reggie.service.OrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @program: reggie_take_out
 * @description:  订单管理
 * @author: zzw
 * @create: 2022-07-27 16:28
 */
@RestController
@RequestMapping("/order")
@Slf4j
@Api("订单管理API接口")
public class OrderController {
    @Autowired
    private OrderService orderService;

    /**
     * 用户下单
     *
     * @param orders
     * @return
     */
    @PostMapping("/submit")
    @ApiOperation(value = "用户下单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orders",value = "orders",required = true,dataType = "Orders")
    })
    public R<String> submit(@RequestBody Orders orders) {
        log.info("订单数据：{}", orders.toString());
        orderService.submit(orders);
        return R.success("下单成功");
    }

    /**
     * 用户订单分页查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/userPage")
    @ApiOperation(value = "用户订单分页查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "page",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize",value = "pageSize",required = true,dataType = "Integer")
    })
    public R<Page> userPage(int page, int pageSize) {
        Page<Orders> pageInfo = orderService.userPage(page, pageSize);
        return R.success(pageInfo);
    }

    /**
     * 订单信息查询
     *
     * @param page
     * @param pageSize
     * @return
     */
    @GetMapping("/page")
    @ApiOperation(value = "订单信息查询")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",value = "page",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "pageSize",value = "pageSize",required = true,dataType = "Integer"),
            @ApiImplicitParam(name = "beginTime",value = "beginTime",required = false,dataType = "String"),
            @ApiImplicitParam(name = "endTime",value = "endTime",required = false,dataType = "String")
    })
    public R<Page> page(int page, int pageSize, @RequestParam(required = false, value = "beginTime") String beginTime, @RequestParam(required = false, value = "endTime") String endTime) {
        log.info("beginTime：{}  endTime：{}", beginTime, endTime);
        Page<Orders> pageInfo = orderService.pageSelect(page, pageSize, beginTime, endTime);
        return R.success(pageInfo);
    }

    /**
     * 派送订单
     *
     * @param orders
     * @return
     */
    @PutMapping
    @ApiOperation(value = "派送订单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "orders",value = "orders",required = true,dataType = "Orders")
    })
    public R<String> delivery(@RequestBody Orders orders) {
        log.info(orders.toString());
        orderService.delivery(orders);
        return R.success("派送成功");
    }
}

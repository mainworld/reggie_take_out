package com.itheima.reggie.dto;

import com.itheima.reggie.entity.Dish;
import com.itheima.reggie.entity.DishFlavor;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜品类的拓展类
 */
@Data
@ApiModel("菜品类的拓展类")
public class DishDto extends Dish {

    /**
     * 该菜品对应的口味集合
     */
    @ApiModelProperty(value = "该菜品对应的口味集合",dataType = "List<DishFlavor>")
    private List<DishFlavor> flavors = new ArrayList<>();

    /**
     * 菜品分类名称
     */
    @ApiModelProperty(value = "菜品分类名称",dataType = "String")
    private String categoryName;

    /**
     * 副本
     */
    @ApiModelProperty(value = "副本",dataType = "Integer")
    private Integer copies;
}

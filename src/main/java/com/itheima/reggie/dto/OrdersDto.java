package com.itheima.reggie.dto;

import com.itheima.reggie.entity.OrderDetail;
import com.itheima.reggie.entity.Orders;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.List;

/**
 * 订单类的拓展类
 */
@Data
@ApiModel("订单类的拓展类")
public class OrdersDto extends Orders {

    /**
     * 用户名称
     */
    @ApiModelProperty(value = "用户名称",dataType = "String")
    private String userName;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",dataType = "String")
    private String phone;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址",dataType = "String")
    private String address;

    /**
     * 受托人
     */
    @ApiModelProperty(value = "受托人",dataType = "String")
    private String consignee;

    /**
     * 订单明细集合
     */
    @ApiModelProperty(value = "订单明细集合",dataType = "List<OrderDetail>")
    private List<OrderDetail> orderDetails;
	
}

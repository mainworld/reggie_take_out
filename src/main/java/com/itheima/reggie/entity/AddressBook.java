package com.itheima.reggie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 地址簿
 */
@Data
@ApiModel("地址簿实体类")
public class AddressBook implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 地址簿id
     */
    @ApiModelProperty(value = "地址簿id",dataType = "Long")
    private Long id;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id",dataType = "Long")
    private Long userId;

    /**
     * 收货人
     */
    @ApiModelProperty(value = "收货人",dataType = "String")
    private String consignee;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",dataType = "String")
    private String phone;

    /**
     * 性别 0 女 1 男
     */
    @ApiModelProperty(value = "性别 0 女 1 男",dataType = "String")
    private String sex;

    /**
     * 省级区划编号
     */
    @ApiModelProperty(value = "省级区划编号",dataType = "String")
    private String provinceCode;

    /**
     * 省级名称
     */
    @ApiModelProperty(value = "省级名称",dataType = "String")
    private String provinceName;

    /**
     * 市级区划编号
     */
    @ApiModelProperty(value = "市级区划编号",dataType = "String")
    private String cityCode;

    /**
     * 市级名称
     */
    @ApiModelProperty(value = "市级名称",dataType = "String")
    private String cityName;

    /**
     * 区级区划编号
     */
    @ApiModelProperty(value = "区级区划编号",dataType = "String")
    private String districtCode;

    /**
     * 区级名称
     */
    @ApiModelProperty(value = "区级名称",dataType = "String")
    private String districtName;

    /**
     * 详细地址
     */
    @ApiModelProperty(value = "详细地址",dataType = "String")
    private String detail;

    /**
     * 标签
     */
    @ApiModelProperty(value = "标签",dataType = "String")
    private String label;

    /**
     * 是否默认 0 否 1是
     */
    @ApiModelProperty(value = "是否默认 0 否 1是",dataType = "Integer")
    private Integer isDefault;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除",dataType = "Integer")
    private Integer isDeleted;
}

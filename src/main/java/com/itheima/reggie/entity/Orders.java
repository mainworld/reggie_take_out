package com.itheima.reggie.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 订单
 */
@Data
@ApiModel("订单实体类")
public class Orders implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id",dataType = "Long")
    private Long id;

    /**
     * 订单号
     */
    @ApiModelProperty(value = "订单号",dataType = "String")
    private String number;

    /**
     * 订单状态 1待付款，2待派送，3已派送，4已完成，5已取消
     */
    @ApiModelProperty(value = "订单状态 1待付款，2待派送，3已派送，4已完成，5已取消",dataType = "Integer")
    private Integer status;

    /**
     * 下单用户id
     */
    @ApiModelProperty(value = "下单用户id",dataType = "Long")
    private Long userId;

    /**
     * 地址id
     */
    @ApiModelProperty(value = "地址id",dataType = "Long")
    private Long addressBookId;

    /**
     * 下单时间
     */
    @ApiModelProperty(value = "下单时间",dataType = "LocalDateTime")
    private LocalDateTime orderTime;

    /**
     * 结账时间
     */
    @ApiModelProperty(value = "结账时间",dataType = "LocalDateTime")
    private LocalDateTime checkoutTime;

    /**
     * 支付方式 1微信，2支付宝
     */
    @ApiModelProperty(value = "支付方式 1微信，2支付宝",dataType = "Integer")
    private Integer payMethod;

    /**
     * 实收金额
     */
    @ApiModelProperty(value = "实收金额",dataType = "BigDecimal")
    private BigDecimal amount;

    /**
     * 备注
     */
    @ApiModelProperty(value = "备注",dataType = "String")
    private String remark;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名",dataType = "String")
    private String userName;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",dataType = "String")
    private String phone;

    /**
     * 地址
     */
    @ApiModelProperty(value = "地址",dataType = "String")
    private String address;

    /**
     * 收货人
     */
    @ApiModelProperty(value = "收货人",dataType = "String")
    private String consignee;
}

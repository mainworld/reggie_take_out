package com.itheima.reggie.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * 订单明细
 */
@Data
@ApiModel("订单明细实体类")
public class OrderDetail implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 订单明细id
     */
    @ApiModelProperty(value = "订单明细id",dataType = "Long")
    private Long id;

    /**
     * 名称
     */
    @ApiModelProperty(value = "名称",dataType = "String")
    private String name;

    /**
     * 订单id
     */
    @ApiModelProperty(value = "订单id",dataType = "Long")
    private Long orderId;

    /**
     * 菜品id
     */
    @ApiModelProperty(value = "菜品id",dataType = "Long")
    private Long dishId;

    /**
     * 套餐id
     */
    @ApiModelProperty(value = "套餐id",dataType = "Long")
    private Long setmealId;

    /**
     * 口味
     */
    @ApiModelProperty(value = "口味",dataType = "String")
    private String dishFlavor;

    /**
     * 数量
     */
    @ApiModelProperty(value = "数量",dataType = "Integer")
    private Integer number;

    /**
     * 金额
     */
    @ApiModelProperty(value = "金额",dataType = "BigDecimal")
    private BigDecimal amount;

    /**
     * 图片
     */
    @ApiModelProperty(value = "图片",dataType = "图片")
    private String image;
}

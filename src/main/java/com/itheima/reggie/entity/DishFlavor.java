package com.itheima.reggie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
菜品口味
 */
@Data
@ApiModel("菜品口味实体类")
public class DishFlavor implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 菜品口味id
     */
    @ApiModelProperty(value = "菜品口味id",dataType = "Long")
    private Long id;

    /**
     * 菜品id
     */
    @ApiModelProperty(value = "菜品id",dataType = "Long")
    private Long dishId;

    /**
     * 口味名称
     */
    @ApiModelProperty(value = "口味名称",dataType = "String")
    private String name;

    /**
     * 口味数据list
     */
    @ApiModelProperty(value = "口味数据list",dataType = "String")
    private String value;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除",dataType = "Integer")
    private Integer isDeleted;
}

package com.itheima.reggie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 套餐菜品关系
 */
@Data
@ApiModel("套餐菜品关系实体类")
public class SetmealDish implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 套餐菜品关系
     */
    @ApiModelProperty(value = "套餐菜品关系",dataType = "Long")
    private Long id;

    /**
     * 套餐id
     */
    @ApiModelProperty(value = "套餐id",dataType = "Long")
    private Long setmealId;

    /**
     * 菜品id
     */
    @ApiModelProperty(value = "菜品id",dataType = "Long")
    private Long dishId;

    /**
     * 菜品名称 （冗余字段）
     */
    @ApiModelProperty(value = "菜品名称 （冗余字段）",dataType = "String")
    private String name;

    /**
     * 菜品原价
     */
    @ApiModelProperty(value = "菜品原价",dataType = "BigDecimal")
    private BigDecimal price;

    /**
     * 份数
     */
    @ApiModelProperty(value = "份数",dataType = "Integer")
    private Integer copies;

    /**
     * 排序
     */
    @ApiModelProperty(value = "排序",dataType = "Integer")
    private Integer sort;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除",dataType = "Integer")
    private Integer isDeleted;
}

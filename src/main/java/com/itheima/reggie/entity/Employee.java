package com.itheima.reggie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 员工实体
 */
@Data
@ApiModel("员工实体类")
public class Employee implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 员工id
     */
    @ApiModelProperty(value = "员工id",dataType = "Long")
    private Long id;

    /**
     * 员工姓名
     */
    @ApiModelProperty(value = "员工姓名",dataType = "String")
    private String username;

    /**
     * 用户名
     */
    @ApiModelProperty(value = "用户名",dataType = "String")
    private String name;

    /**
     * 密码
     */
    @ApiModelProperty(value = "密码",dataType = "String")
    private String password;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",dataType = "String")
    private String phone;

    /**
     * 性别
     */
    @ApiModelProperty(value = "性别",dataType = "String")
    private String sex;

    /**
     * 身份证号码
     */
    @ApiModelProperty(value = "身份证号码",dataType = "String")
    private String idNumber;

    /**
     * 账号状态
     */
    @ApiModelProperty(value = "账号状态",dataType = "Integer")
    private Integer status;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT)  //插入时填充字段
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT_UPDATE) //插入和更新时填充字段
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT) //插入时填充字段
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT_UPDATE) //插入和更新时填充字段
    private Long updateUser;
}

package com.itheima.reggie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 菜品
 */
@Data
@ApiModel("菜品实体类")
public class Dish implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 菜品id
     */
    @ApiModelProperty(value = "菜品id",dataType = "Long")
    private Long id;

    /**
     * 菜品名称
     */
    @ApiModelProperty(value = "菜品名称",dataType = "String")
    private String name;

    /**
     * 菜品分类id
     */
    @ApiModelProperty(value = "菜品分类id",dataType = "Long")
    private Long categoryId;

    /**
     * 菜品价格
     */
    @ApiModelProperty(value = "菜品价格",dataType = "BigDecimal")
    private BigDecimal price;

    /**
     * 商品码
     */
    @ApiModelProperty(value = "商品码",dataType = "String")
    private String code;

    /**
     * 图片
     */
    @ApiModelProperty(value = "图片",dataType = "String")
    private String image;

    /**
     * 描述信息
     */
    @ApiModelProperty(value = "描述信息",dataType = "String")
    private String description;

    /**
     * 0 停售 1 起售
     */
    @ApiModelProperty(value = "0 停售 1 起售",dataType = "Integer")
    private Integer status;

    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序",dataType = "Integer")
    private Integer sort;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除",dataType = "Integer")
    private Integer isDeleted;
}

package com.itheima.reggie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * 套餐
 */
@Data
@ApiModel("套餐实体类")
public class Setmeal implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 套餐id
     */
    @ApiModelProperty(value = "套餐id",dataType = "Long")
    private Long id;

    /**
     * 分类id
     */
    @ApiModelProperty(value = "分类id",dataType = "Long")
    private Long categoryId;

    /**
     * 套餐名称
     */
    @ApiModelProperty(value = "套餐名称",dataType = "String")
    private String name;

    /**
     * 套餐价格
     */
    @ApiModelProperty(value = "套餐价格",dataType = "BigDecimal")
    private BigDecimal price;

    /**
     * 状态 0:停用 1:启用
     */
    @ApiModelProperty(value = "状态 0:停用 1:启用",dataType = "Integer")
    private Integer status;

    /**
     * 编码
     */
    @ApiModelProperty(value = "编码",dataType = "String")
    private String code;

    /**
     * 描述信息
     */
    @ApiModelProperty(value = "描述信息",dataType = "String")
    private String description;

    /**
     * 图片
     */
    @ApiModelProperty(value = "图片",dataType = "String")
    private String image;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

    /**
     * 是否删除
     */
    @ApiModelProperty(value = "是否删除",dataType = "Integer")
    private Integer isDeleted;
}

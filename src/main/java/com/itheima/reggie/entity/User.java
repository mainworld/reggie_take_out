package com.itheima.reggie.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
/**
 * 用户信息
 */
@Data
@ApiModel("用户实体类")
public class User implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    @ApiModelProperty(value = "用户id",dataType = "Long")
    private Long id;

    /**
     * 姓名
     */
    @ApiModelProperty(value = "姓名",dataType = "String")
    private String name;

    /**
     * 手机号
     */
    @ApiModelProperty(value = "手机号",dataType = "String")
    private String phone;

    /**
     * 性别 0 女 1 男
     */
    @ApiModelProperty(value = "性别 0 女 1 男",dataType = "String")
    private String sex;

    /**
     * 身份证号
     */
    @ApiModelProperty(value = "身份证号",dataType = "String")
    private String idNumber;

    /**
     * 头像
     */
    @ApiModelProperty(value = "头像",dataType = "String")
    private String avatar;

    /**
     * 状态 0:禁用，1:正常
     */
    @ApiModelProperty(value = "状态 0:禁用，1:正常",dataType = "Integer")
    private Integer status;
}

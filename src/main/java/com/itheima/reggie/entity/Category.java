package com.itheima.reggie.entity;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 分类
 */
@Data
@ApiModel("分类实体类")
public class Category implements Serializable {

    /**
     * 序列化id
     */
    private static final long serialVersionUID = 1L;

    /**
     * 分类id
     */
    @ApiModelProperty(value = "分类id",dataType = "Long")
    private Long id;

    /**
     * 类型 1 菜品分类 2 套餐分类
     */
    @ApiModelProperty(value = "类型 1 菜品分类 2 套餐分类",dataType = "Integer")
    private Integer type;

    /**
     * 分类名称
     */
    @ApiModelProperty(value = "分类名称",dataType = "String")
    private String name;

    /**
     * 顺序
     */
    @ApiModelProperty(value = "顺序",dataType = "Integer")
    private Integer sort;

    /**
     * 创建时间
     */
    @ApiModelProperty(value = "创建时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createTime;

    /**
     * 更新时间
     */
    @ApiModelProperty(value = "更新时间",dataType = "LocalDateTime")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updateTime;

    /**
     * 创建人
     */
    @ApiModelProperty(value = "创建人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT)
    private Long createUser;

    /**
     * 修改人
     */
    @ApiModelProperty(value = "修改人",dataType = "Long")
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Long updateUser;

    //是否删除
//    private Integer isDeleted;

}

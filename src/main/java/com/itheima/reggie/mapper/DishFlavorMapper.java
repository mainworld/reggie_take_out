package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.DishFlavor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: reggie_take_out
 * @description:
 * @author: zzw
 * @create: 2022-07-25 14:40
 */
@Mapper
public interface DishFlavorMapper extends BaseMapper<DishFlavor> {
}

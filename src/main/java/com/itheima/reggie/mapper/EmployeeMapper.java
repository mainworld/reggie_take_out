package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Employee;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: reggie_take_out
 * @description:
 * @author: zzw
 * @create: 2022-07-22 16:22
 */
@Mapper
public interface EmployeeMapper extends BaseMapper<Employee> {
}

package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.SetmealDish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: reggie_take_out
 * @description:
 * @author: zzw
 * @create: 2022-07-26 14:56
 */
@Mapper
public interface SetmealDishMapper extends BaseMapper<SetmealDish> {
}

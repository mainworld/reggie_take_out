package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.Dish;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: reggie_take_out
 * @description:
 * @author: zzw
 * @create: 2022-07-23 23:20
 */
@Mapper
public interface DishMapper extends BaseMapper<Dish> {
}

package com.itheima.reggie.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.reggie.entity.AddressBook;
import org.apache.ibatis.annotations.Mapper;

/**
 * @program: reggie_take_out
 * @description:
 * @author: zzw
 * @create: 2022-07-27 15:06
 */
@Mapper
public interface AddressBookMapper extends BaseMapper<AddressBook> {
}

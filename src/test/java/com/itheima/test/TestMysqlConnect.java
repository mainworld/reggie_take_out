package com.itheima.test;

import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.SQLException;

/**
 * @program: reggie_take_out
 * @description: 测试mysql连接
 * @author: zzw
 * @create: 2022-08-16 10:06
 */
@Slf4j
@SpringBootTest(classes = com.itheima.reggie.ReggieApplication.class)
public class TestMysqlConnect {
    @Resource
    private DataSource dataSource;
    @Test
    public void test() throws SQLException {
        log.info("连接为：{}",dataSource.getConnection());
    }
}

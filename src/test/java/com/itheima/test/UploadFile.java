package com.itheima.test;

import com.itheima.reggie.ReggieApplication;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import java.io.File;

/**
 * @program: reggie_take_out
 * @description:  测试获取文件名
 * @author: zzw
 * @create: 2022-07-25 11:32
 */
@Slf4j
public class UploadFile {
    @Test
    public void test1(){
        String fileName="dfdf.jpg";
        String suffix=fileName.substring(fileName.lastIndexOf("."));
        System.out.println(suffix);
    }

    @Test
    public void test2(){
        String rootPath= ReggieApplication.class.getClassLoader().getResource(".").getFile();
        File classFile = new File(rootPath);

        String path=classFile.getParentFile().getParent();
        log.info("项目绝对路径：{}",path);
    }
}
